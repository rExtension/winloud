![Title](res/image/splash/title.webp)

> “三国时代覆灭后，某位五胡乱华中幸存的一位士兵捡到了破损的太平天书......随后意外重启了三国！
>
> ”但是，这次的三国，好像不太一样......“

本扩展原为rExtension系列扩展之一，现已为rExtension组所创建并维护的主要扩展之一。

本扩展拥有以下元素:
- 对原始武将的重塑
- 修改或创造的卡牌
- 收集与解密系统
- 实体卡牌的电子收藏

> 目前扩展正在更新当中，尽情期待

希望大家能在扩展中玩得开心，更希望有人能从扩展中有所收获。

> Version: Build 1

> ## Authors:
> - Rintim \<rintim@foxmail.com\>
> - Angel \<3371152010@qq.com\>
>
> 扩展QQ群: 624482093

---

# 扩展目标（goal）

- 武将设计
- 卡牌设计
- 天书碎片系统
- etc.
  
# 扩展非目标（non-goal）

- 完整的剧情（不排除微剧情）

# 下载地址

> 待定

# 关于贡献

在gitlab/github中，你可以随时fork仓库并提交pull request/merged request，我们会根据扩展风格与走向来适当采纳并改写代码。

在QQ群中，你可以上传你修改过的扩展并提醒我们，我们同样会检索代码并将代码融入到扩展当中。

如果你在游玩过程中遇到bug，你可以在QQ群反馈，或者提issues。我们会不定期修复issues中出现的bug（如果我们会的话）

# Why isn't the readme written in English

The game "noname" is not written in English, and there is no options to switch the language as there are not any English words in the source. So we don't think this extension for noname needs to have english documents.