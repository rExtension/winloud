// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: wind.js (rExtension/extension-winloud/source/character/wind.js)
// Content:  风轻云淡 万物开端栏武将信息
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

module.exports = ({ translate, dot }) => ({
	character: {
		// 曹魏
		// Draw Pie
		"rExtension_rEyd-WEI008": {
			translate: "夏侯渊",
			title: "神速的獵豹",
			sex: "male",
			group: "wei",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "xiahouyuan",
			skills: ["rExtension_winloud_jisu", "rExtension_winloud_shebian"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-WEI008${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-WEI008.ogg`,
			],
			des: "字妙才，夏侯惇族弟，沛国谯县（今安徽省亳州市）人。东汉末年名将。随曹操起兵，从征袁绍、韩遂等。建安十七年（212）后，督军屯于长安，平定河西诸羌，其勇名号称“虎步关右，所向无前。”。建安二十年（215）拜征西将军，守汉中，后为刘备部将黄忠所击杀，死后追谥为愍侯。用兵擅长轻兵急袭、出敌不意，亦常恃勇无谋亲率轻锐出战，也因此常受曹操劝诫，军中称之为“白地将军”。 ",
			catelogy: "wind"
		}
	},
	skill: {
		"rExtension_winloud_jisu": {
			name: "疾速",
			des: "出牌阶段限一次，你可以将手牌弃至体力值（不足则不弃），然后视为对其使用一张【杀】；当你受到当前回合角色造成的伤害时，你可视为对其使用一张【杀】",
			content: {
				audio: ["jisu", `ext:${translate}/res/audio/skill:2`],
				group: ["rExtension_winloud_jisu_phaseUse", "rExtension_winloud_jisu_damageEnd"]
			}
		},
		"rExtension_winloud_jisu_phaseUse": {
			name: "疾速",
			des: "出牌阶段限一次，你可以将手牌弃至体力值（不足则不弃），你可以选择一名攻击范围内的其他角色，视为对其使用一张【杀】",
			content: {
				audio: ["jisu", `ext:${translate}/res/audio/skill:2`],
				usable: 1,
				enable: "phaseUse",
				filter: (_event, player, _name) => game.hasPlayer(current => player.inRange(current) && player.canUse("sha", current, false)),
				filterTarget: (_card, player, target) => player.inRange(target) && player.canUse("sha", target, false),
				selectTarget: 1,
				filterCard: (..._args) => {
					const player = _status.event.player;
					return player.countCards("h") > player.hp;
				},
				selectCard: () => {
					const player = _status.event.player;
					if (player.countCards("h") <= player.hp) return -1;
					return player.countCards("h") - player.hp;
				},
				position: "h",
				content: () => {
					player.useCard({ name: "sha", isCard: true }, false, targets).card.jisu = true;
				},
				ai: {
					order: () => get.order({ name: "sha" }) - 0.4,
					result: {
						target: (player, target) => {
							const eff = get.effect(target, { name: "sha" }, player, target);
							const damageEff = get.damageEffect(target, player, player);
							if (eff > 0) return damageEff > 0 ? 0 : eff;
							if (target.hasSkill("bagua_skill") || target.hasSkill("rw_bagua_skill") || target.hasSkill("bazhen")) return 0;
							return eff;
						},
					},
				},
			}
		},
		"rExtension_winloud_jisu_damageEnd": {
			name: "疾速",
			des: "当你受到当前回合角色造成的伤害时，你可视为对其使用一张【杀】",
			content: {
				audio: ["jisu", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "damageEnd"
				},
				filter: (event, player, _name) => event.source && event.source == _status.currentPhase && player.canUse("sha", event.source, false),
				logTarget: "source",
				content: () => {
					player.useCard({ name: "sha", isCard: true }, false, trigger.source).card.jisu = true;
				}
			}
		},
		"rExtension_winloud_shebian": {
			name: "设变",
			des: "锁定技，当你使用无实体牌的【杀】结算或造成伤害后，终止一切结算且当前回合结束。",
			desc: "<b>锁定技，</b>当你使用无实体牌的【杀】结算或造成伤害后，终止一切结算且当前回合结束。",
			content: {
				audio: ["shebian", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "useCardAfter",
					source: "damageSource"
				},
				locked: true,
				forced: true,
				filter: (event, _player, _name) => event.card && event.card.name == "sha" && (!event.cards || !event.cards.length),
				content: () => {
					"step 0"
					const cards = Array.from(ui.ordering.childNodes);
					cards.forEach(card => card.discard());
					"step 1"
					const evt = _status.event.getParent("phase");
					if (evt) {
						game.resetSkills();
						_status.event = evt;
						_status.event.finish();
						_status.event.untrigger(true);
					}
				}
			}
		}
	}
});  